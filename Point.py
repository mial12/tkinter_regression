class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def draw(self, canvas, fill='black', outline='black', point_size = 1):
        x = self.x + 200
        y = 200 - self.y
        x1, y1 = (x - point_size), (y - point_size)
        x2, y2 = (x + point_size), (y + point_size)
        canvas.create_oval(x1, y1, x2, y2, fill=fill, outline=outline)

    def __str__(self):
        return f"({str(self.x)}, {str(self.y)})"

    def __repr__(self):
        return str(self)




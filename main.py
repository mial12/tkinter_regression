import tkinter as tk
import numpy as np
from sklearn import linear_model
from sklearn.preprocessing import PolynomialFeatures
from typing import List
from Point import Point


class App(tk.Tk):

    def __init__(self):
        # call super constructor
        super().__init__()

        # create canvas
        self.canvas = tk.Canvas(self, height=400, width=400, borderwidth=2, relief="groove")
        self.canvas.bind("<Button-1>", self.canvas_clicked_event)
        self.canvas.pack()

        # create lines
        self.canvas.create_line(0, 200, 400, 200)
        self.canvas.create_line(200, 0, 200, 400)

        # create clear button
        self.clear_button = tk.Button(self, text="Clear")
        self.clear_button.bind("<Button-1>", self.clear_points)
        self.clear_button.pack()

        # create regression selector
        tk.Label(self, text="Select regression type ").pack(anchor='w')

        self.regression_selector = tk.StringVar(self, "Linear")
        tk.Radiobutton(self, text="Linear", variable=self.regression_selector, value="Linear", command=self.get_regression_selector).pack(anchor='w')
        tk.Radiobutton(self, text="Quadratic", variable=self.regression_selector, value="Quadratic", command=self.get_regression_selector).pack(anchor='w')
        tk.Radiobutton(self, text="Cubic", variable=self.regression_selector, value="Cubic", command=self.get_regression_selector).pack(anchor='w')

        # create plot button 
        self.plot_button = tk.Button(self, text="Plot")
        self.plot_button.bind("<Button-1>", self.plot)
        self.plot_button.pack()

        # define array of points
        self.points: List[Point] = []

    def get_regression_selector(self):
        return self.regression_selector.get()

    def canvas_clicked_event(self, event):
        x = event.x
        y = event.y

        p = Point(x-200, -(y-200))
        p.draw(self.canvas, fill=None, point_size=3)

        self.points.append(p)

    def plot(self, event):
        x_array = []
        y_array = []
        for p in self.points:
            x_array.append(p.x)
            y_array.append(p.y)

        x_array = np.array(x_array)
        x_array = x_array.reshape(-1, 1)

        y_array = np.array(y_array)
        y_array = y_array.reshape(-1, 1)

        if self.get_regression_selector() == 'Linear':
            lm = linear_model.LinearRegression(fit_intercept=True)
            lm.fit(x_array, y_array)

            a = lm.coef_[0][0]
            b = lm.intercept_[0]

            point_list = []
            for i in range(-800, 800):
                x = float(i) / 4
                y = a * x + b
                point = Point(x, y)
                point_list.append(point)

            for point in point_list:
                point.draw(self.canvas, fill='blue', outline='blue')

        elif self.get_regression_selector() == 'Quadratic':
            quadratic = PolynomialFeatures(degree=2, include_bias=False)
            quadratic_x = quadratic.fit_transform(x_array)
            lm = linear_model.LinearRegression()
            lm.fit(quadratic_x, y_array)

            a = lm.coef_[0][0]
            a2 = lm.coef_[0][1]
            b = lm.intercept_[0]

            point_list = []
            for i in range(-800, 800):
                x = float(i) / 4
                y = a * x + a2 * x ** 2 + b
                point = Point(x, y)
                point_list.append(point)

            for point in point_list:
                point.draw(self.canvas, fill='green', outline='green')

        else:
            cubic = PolynomialFeatures(degree=3, include_bias=False)
            cubic_x = cubic.fit_transform(x_array)
            lm = linear_model.LinearRegression()
            lm.fit(cubic_x, y_array)

            a = lm.coef_[0][0]
            a2 = lm.coef_[0][1]
            a3 = lm.coef_[0][2]
            b = lm.intercept_[0]

            point_list = []
            for i in range(-800, 800):
                x = float(i) / 4
                y = a * x + a2 * x ** 2 + a3 * x ** 3 + b
                point = Point(x, y)
                point_list.append(point)

            for point in point_list:
                point.draw(self.canvas, fill='sienna4', outline='sienna4')


    def clear_points(self, event):
        self.canvas.delete("all")
        self.points.clear()

        self.canvas.create_line(0, 200, 400, 200)
        self.canvas.create_line(200, 0, 200, 400)


if __name__ == "__main__":
    app = App()
    app.mainloop()
